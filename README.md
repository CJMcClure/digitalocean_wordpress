###[Server Setup...](setup.md)

#Deploying changes to server

###Feature Branch Workflow

Open a terminal on your local machine and enter the following command:

```Shell
cd /Your/project
```
```
git checkout -b release
```
> Note: This branch will be pushed to the server when you merge your feature branch

```
git checkout -b <name-of-feature-branch>
```
> Note: This is where we develop our new feature

Once you have made a change, git add and git commit

```Shell
git add <your-files>
```
```Shell
git commit -m "Your message goes here"
```

Once you are finished with your feature, merge your branches

```Shell
git checkout release
```
```Shell
git merge feature
```





