#**Server Setup - Digital Ocean**

##Droplet

###Sign Up

Setup an account at [Digital Ocean](https://www.digitalocean.com)

> Once you are logged in, complete the following steps from your dashboard.

###Initialize

Click the `Create Droplet` button.

Select `Ubuntu 16.04.2x64`

Select `$5/mth` size package

Select a `New York` data center

Name Droplet `<droptlet-name>`

Click `Create`

###Setup

Your droplet `<droplet-name>` should now be visible on your dashboard, under **Droplets**.

> Note: If you do not have a droplet on your dashboard, repeat the Initialize steps above

Click the `<droplet-name>` droplet to access the droplet dashboard.

Copy the `ipv4` address from the menu.

Use your favorite browser to navigate to your droplet's `ipv4` address.

> Note: A root password was sent to your email, you will need this password to ssh into your server to complete all further steps. 

#**Server Setup - SSH**

##Connect to your server via SSH

Open a new `terminal` window on your computer.

To ssh into your server, type the following commands into your terminal:

```
ssh root@<server-ipv4-address>
```
You will be prompted for your password, this is the password you recieved in your email.

> Note: You can reset your root password from the `access` option in your **Digital Ocean** droplet dashboard side-bar options. 

After you enter your password, your terminal will transition into your servers terminal. All commands entered here will be run on your server and only on your server, until you type `exit`, which will terminate your ssh connection, bringing you back to your local terminal.

>Note: Every installation from this point forward will be performed through your ssh connection, on your server. This is now implied and will not be mentioned again.

##Create non-root user

Enter the following commands into your terminal:

```
useradd <new-user-name>
```
After being prompted for and providing a new password, switch users:

```
su <new-user-name>
```

#Server Setup - Stack Intallation

> Note: All proceeding steps will assume you are operating from the server terminal, which can be accessed via ssh

##NGINX

###Install

```Shell
sudo apt-get update
```
```Shell
sudo apt-get install nginx
```

###Test

To test the success of your NGINX installation, type the following command into terminal: 

```Shell
nginx -t
```
> Note: you should receive the following response:

> nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
> nginx: configuration file /etc/nginx/nginx.conf test is successful

##MariaDB

###Install

Enter the following commands into your terminal:

```
apt-get update -y
```

```
apt-get install mariadb-server
```

###Configure

```
mysql_secure_installation
```

###Test 

```
mysql -u root -p
```

> Note: If you can log in with the above command, the installation and configuration were completed successfully.

##PHP7.0

###Install

Enter the following commands into your terminal:

```
sudo add-apt-repository ppa:ondrej/php
```

```
sudo apt-get update
```

```
sudo apt-get install php7.0-fpm php7.0-mbstring php7.0-xml php7.0-mysql php7.0-common php7.0-gd php7.0-json php7.0-cli php7.0-curl
```

###Configuration

```
sudo mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak
```
```
sudo touch /etc/nginx/sites-available/default
```
Copy the following code block to your clipboard:

```
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root /var/www/html;
    index index.php index.html index.htm index.nginx-debian.html;

    server_name server_domain_or_IP;

    location / {
<<<<<<< HEAD
        #try_files $uri $uri/ =404;
        try_files $uri $uri/ /index.php?q=$uri&$args;
=======
        try_files $uri $uri/ =404;
>>>>>>> feature
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
    }

    location ~ /\.ht {
        deny all;
    }
}
```
Enter this command into the terminal:
 
```
sudo nano /etc/nginx/sites-available/default
```

Paste the code snippet you copied from above into the file and then Hit `control + x`, `y`, & `return`.

```
sudo systemctl restart nginx
```
> Note: We will test the php configuration after we setup wordpress

##Wordpress

###MySQL

Login to your MySQL server

```
mysql -u root -p
```
Run the following queries 

```
CREATE USER <new-username>@localhost IDENTIFIED BY '<new-password>';
```
> Note: Replace `<new-username>` and `<new-password>` with values of your choosing.

```
GRANT ALL PRIVILEGES ON wordpress.* TO <new-username>@localhost;
```
```
FLUSH PRIVILEGES;
```
```
exit
```
> Note: Exit should take you out of mysql and back to your servers terminal before proceeding.

###Download & Install Wordpress

Enter the following commands into the terminal:

```
cd ~
```
```
wget http://wordpress.org/latest.tar.gz
```
```
tar xzvf latest.tar.gz
```
<<<<<<< HEAD
```
sudo apt-get update
```
=======
>>>>>>> feature

###Configuration

```
cd ~/wordpress
```
```
cp wp-config-sample.php wp-config.php
```
```
nano wp-config.php
```

Once the `wp-config.php` file is open in nano, you need to update the following three database fields:

```
define('DB_NAME', 'wordpress');
```
```
define('DB_USER', '<new-user>');
``` 
> Note: Replace `<new-user>` with the user you created in the **MySQL** section.

```
define('DB_PASSWORD', '<new-user-password>');
```
> Note: Replace `<new-user-password>` with the password you used in the **MySQL** section above.

After updating the fields, hit `control + x`, `y`, & `return`.

```
mkdir wp-content/uploads
```

###RSYNC Wordpress install to NGINX directory with default permissions

```
sudo rsync -avP ~/wordpress/ /var/www/html
```

###Add Non-Root user to www-data group

```
sudo chown -R <non-root-username>:www-data /var/www/html/*
```
> Note: Switch out `<non-root-username>` with the user you created in the **Create Non-Root User** section



